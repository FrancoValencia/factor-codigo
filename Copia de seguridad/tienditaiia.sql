-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 29-04-2016 a las 16:14:15
-- Versión del servidor: 5.5.47-0ubuntu0.14.04.1
-- Versión de PHP: 5.5.9-1ubuntu4.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `tienditaiia`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categoria`
--

CREATE TABLE IF NOT EXISTS `categoria` (
  `id_categoria` int(4) NOT NULL AUTO_INCREMENT,
  `nombre_categoria` varchar(30) CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL,
  PRIMARY KEY (`id_categoria`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf32 COLLATE=utf32_spanish2_ci AUTO_INCREMENT=5 ;

--
-- Volcado de datos para la tabla `categoria`
--

INSERT INTO `categoria` (`id_categoria`, `nombre_categoria`) VALUES
(1, 'lácteos'),
(2, 'carnicos'),
(3, 'frutas & hortalizas'),
(4, 'cereales');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `lote`
--

CREATE TABLE IF NOT EXISTS `lote` (
  `id_lote` int(4) NOT NULL AUTO_INCREMENT,
  `fecha_caducidad` datetime NOT NULL,
  `fecha_elaboracion` datetime NOT NULL,
  `cantidad_lote` int(4) NOT NULL,
  `id_usuario` int(4) NOT NULL,
  `id_producto` int(4) NOT NULL,
  PRIMARY KEY (`id_lote`),
  KEY `usuariolote` (`id_usuario`),
  KEY `productolote` (`id_producto`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci AUTO_INCREMENT=78 ;

--
-- Volcado de datos para la tabla `lote`
--

INSERT INTO `lote` (`id_lote`, `fecha_caducidad`, `fecha_elaboracion`, `cantidad_lote`, `id_usuario`, `id_producto`) VALUES
(1, '2001-10-16 10:00:00', '2001-01-16 10:00:00', 12, 16, 1),
(2, '2001-10-16 10:05:00', '2001-01-16 10:00:00', 12, 37, 2),
(3, '2001-10-16 10:10:00', '2001-01-16 10:00:00', 12, 7, 3),
(4, '2001-10-16 10:15:00', '2001-01-16 10:00:00', 12, 37, 4),
(5, '2001-10-16 10:20:00', '2001-01-16 10:00:00', 12, 34, 5),
(6, '2001-10-16 10:25:00', '2001-01-16 10:00:00', 12, 28, 6),
(7, '2001-10-16 10:30:00', '2001-01-16 10:00:00', 12, 4, 7),
(8, '2001-10-16 10:35:00', '2001-01-16 10:00:00', 12, 12, 8),
(9, '2001-10-16 10:40:00', '2001-01-16 10:00:00', 12, 40, 9),
(10, '2001-10-16 10:45:00', '2001-01-16 10:00:00', 18, 12, 10),
(11, '2001-10-16 10:50:00', '2001-01-16 10:00:00', 18, 28, 11),
(12, '2001-10-16 10:55:00', '2001-01-16 10:00:00', 18, 11, 12),
(13, '2001-10-16 11:00:00', '2001-01-16 10:00:00', 18, 6, 13),
(14, '2001-10-16 11:05:00', '2001-01-16 10:00:00', 18, 44, 14),
(15, '2001-10-16 11:10:00', '2001-01-16 10:00:00', 18, 19, 15),
(16, '2001-10-16 11:15:00', '2001-01-16 10:00:00', 18, 8, 16),
(17, '2001-10-16 11:20:00', '2001-01-16 10:00:00', 18, 1, 17),
(18, '2001-10-16 11:25:00', '2001-01-16 10:00:00', 18, 38, 18),
(19, '2001-10-16 11:30:00', '2001-01-16 10:00:00', 18, 18, 19),
(20, '2002-10-16 10:00:00', '2001-01-16 10:00:00', 8, 31, 20),
(21, '2002-10-16 10:05:00', '2001-01-16 10:00:00', 8, 3, 21),
(22, '2002-10-16 10:10:00', '2001-01-16 10:00:00', 8, 9, 22),
(23, '2002-10-16 10:15:00', '2001-01-16 10:00:00', 8, 9, 23),
(24, '2002-10-16 10:20:00', '2001-01-16 10:00:00', 8, 46, 24),
(25, '2002-10-16 10:25:00', '2001-01-16 10:00:00', 8, 12, 25),
(26, '2002-10-16 10:30:00', '2001-01-16 10:00:00', 8, 29, 26),
(27, '2002-10-16 10:35:00', '2001-01-16 10:00:00', 8, 7, 27),
(28, '2002-10-16 10:40:00', '2001-01-16 10:00:00', 8, 34, 28),
(29, '2002-10-16 10:45:00', '2001-01-16 10:00:00', 8, 47, 29),
(30, '2002-10-16 10:50:00', '2001-01-16 10:00:00', 8, 22, 30),
(31, '2002-10-16 10:55:00', '2001-01-16 10:00:00', 8, 42, 31),
(32, '2002-10-16 11:00:00', '2001-01-16 10:00:00', 8, 23, 32),
(33, '2002-10-16 11:05:00', '2001-01-16 10:00:00', 8, 16, 33),
(34, '2002-10-16 11:10:00', '2001-01-16 10:00:00', 8, 22, 34),
(35, '2002-10-16 11:15:00', '2001-01-16 10:00:00', 8, 30, 35),
(36, '2002-10-16 11:20:00', '2001-01-16 10:00:00', 8, 4, 36),
(37, '2002-10-16 11:25:00', '2001-01-16 10:00:00', 8, 2, 37),
(38, '2002-10-16 11:30:00', '2001-01-16 10:00:00', 8, 31, 38),
(39, '2003-10-16 10:00:00', '2001-01-16 10:00:00', 8, 13, 39),
(40, '2003-10-16 10:05:00', '2001-01-16 10:00:00', 8, 15, 40),
(41, '2003-10-16 10:10:00', '2001-01-16 10:00:00', 8, 9, 41),
(42, '2003-10-16 10:15:00', '2001-01-16 10:00:00', 8, 2, 42),
(43, '2003-10-16 10:20:00', '2001-01-16 10:00:00', 8, 22, 43),
(44, '2003-10-16 10:25:00', '2001-01-16 10:00:00', 8, 10, 44),
(45, '2003-10-16 10:30:00', '2001-01-16 10:00:00', 8, 19, 45),
(46, '2003-10-16 10:35:00', '2001-01-16 10:00:00', 8, 49, 46),
(47, '2003-10-16 10:40:00', '2001-01-16 10:00:00', 8, 24, 47),
(48, '2003-10-16 10:45:00', '2001-01-16 10:00:00', 8, 39, 48),
(49, '2003-10-16 10:50:00', '2001-01-16 10:00:00', 8, 43, 49),
(50, '2003-10-16 10:55:00', '2001-01-16 10:00:00', 8, 28, 50),
(51, '2003-10-16 11:00:00', '2001-01-16 10:00:00', 8, 39, 51),
(52, '2003-10-16 11:05:00', '2001-01-16 10:00:00', 8, 4, 52),
(53, '2003-10-16 11:10:00', '2001-01-16 10:00:00', 8, 4, 53),
(54, '2003-10-16 11:15:00', '2001-01-16 10:00:00', 8, 34, 54),
(55, '2003-10-16 11:20:00', '2001-01-16 10:00:00', 8, 16, 55),
(56, '2003-10-16 11:25:00', '2001-01-16 10:00:00', 8, 10, 56),
(57, '2003-10-16 11:30:00', '2001-01-16 10:00:00', 8, 43, 57),
(58, '2004-10-16 10:00:00', '2001-01-16 10:00:00', 8, 44, 58),
(59, '2004-10-16 10:05:00', '2001-01-16 10:00:00', 8, 24, 59),
(60, '2004-10-16 10:10:00', '2001-01-16 10:00:00', 14, 37, 60),
(61, '2004-10-16 10:15:00', '2001-01-16 10:00:00', 14, 26, 61),
(62, '2004-10-16 10:20:00', '2001-01-16 10:00:00', 14, 12, 62),
(63, '2004-10-16 10:25:00', '2001-01-16 10:00:00', 14, 30, 63),
(64, '2004-10-16 10:30:00', '2001-01-16 10:00:00', 14, 25, 64),
(65, '2004-10-16 10:35:00', '2001-01-16 10:00:00', 14, 41, 65),
(66, '2004-10-16 10:40:00', '2001-01-16 10:00:00', 14, 10, 66),
(67, '2004-10-16 10:45:00', '2001-01-16 10:00:00', 14, 50, 67),
(68, '2004-10-16 10:50:00', '2001-01-16 10:00:00', 14, 34, 68),
(69, '2004-10-16 10:55:00', '2001-01-16 10:00:00', 14, 14, 69),
(70, '2004-10-16 11:00:00', '2001-01-16 10:00:00', 10, 33, 70),
(71, '2004-10-16 11:00:00', '2001-01-16 10:00:00', 10, 33, 71),
(72, '2004-10-16 11:00:00', '2001-01-16 10:00:00', 10, 33, 72),
(73, '2004-10-16 11:00:00', '2001-01-16 10:00:00', 10, 33, 73),
(74, '2004-10-16 11:00:00', '2001-01-16 10:00:00', 10, 33, 74),
(75, '2004-10-16 11:00:00', '2001-01-16 10:00:00', 10, 33, 75),
(76, '2012-01-16 10:00:00', '2001-01-16 10:00:00', 12, 1, 1),
(77, '2016-08-09 00:00:00', '2015-02-03 00:00:00', 100, 1, 11);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `merma`
--

CREATE TABLE IF NOT EXISTS `merma` (
  `id_merma` int(4) NOT NULL AUTO_INCREMENT,
  `fecha_merma` datetime NOT NULL,
  `descripcion_merma` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  `id_usuario` int(4) NOT NULL,
  `id_producto` int(4) NOT NULL,
  `cantidad` int(4) NOT NULL,
  PRIMARY KEY (`id_merma`),
  KEY `usuariomerma` (`id_usuario`),
  KEY `mermaproducto` (`id_producto`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci AUTO_INCREMENT=16 ;

--
-- Volcado de datos para la tabla `merma`
--

INSERT INTO `merma` (`id_merma`, `fecha_merma`, `descripcion_merma`, `id_usuario`, `id_producto`, `cantidad`) VALUES
(1, '2016-04-25 00:00:00', 'caduco', 1, 1, 5),
(2, '2016-04-26 20:56:21', 'Producto robado', 2, 3, 10),
(7, '2016-04-26 05:17:11', 'Producto robado', 1, 3, 1),
(8, '2016-04-26 05:18:29', 'Producto robado', 1, 1, 3),
(9, '2016-04-26 17:46:48', 'Producto robado', 1, 3, 1),
(10, '2016-04-26 18:07:16', 'Producto dañado', 1, 1, 6),
(11, '2016-04-27 23:47:52', 'Producto dañado', 1, 1, 0),
(12, '2016-04-27 23:55:47', 'Producto dañado', 1, 1, 0),
(13, '2016-04-28 04:05:17', 'Producto dañado', 1, 75, 0),
(14, '2016-04-28 04:22:55', 'Producto dañado', 1, 1, 1),
(15, '2016-04-28 05:27:05', 'Producto robado', 1, 3, 20);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `privilegios`
--

CREATE TABLE IF NOT EXISTS `privilegios` (
  `id_privilegio` int(3) NOT NULL AUTO_INCREMENT,
  `nombre_privilegio` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  PRIMARY KEY (`id_privilegio`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci AUTO_INCREMENT=31 ;

--
-- Volcado de datos para la tabla `privilegios`
--

INSERT INTO `privilegios` (`id_privilegio`, `nombre_privilegio`) VALUES
(1, 'Registrar cuenta como cliente'),
(2, 'Iniciar sesion'),
(3, 'Consultar productos'),
(4, 'Consultar comentarios'),
(5, 'Consultar apartados activos'),
(6, 'Apartar producto'),
(7, 'Cancelar producto apartado'),
(8, 'Consultar apartados realizados anteriormente'),
(9, 'Modificar perfil'),
(10, 'Registrar comentario'),
(11, 'Evaluar producto'),
(12, 'Visualizar evaluaciones'),
(13, 'Consultar historial de apartados'),
(14, 'Modificar estado del apartado'),
(15, 'Registrar merma'),
(16, 'Consultar inventario actual'),
(17, 'Registrar venta'),
(18, 'Modificar venta'),
(19, 'Consultar historial de ventas'),
(20, 'Registrar productos'),
(21, 'Modificar productos'),
(22, 'Eliminar productos'),
(23, 'Generar reporte de inventario actual'),
(24, 'Generar reporte dia'),
(25, 'Generar reporte mes'),
(26, 'Generar reporte semana'),
(27, 'Registrar empleado'),
(28, 'Modificar empleado'),
(29, 'Eliminar empleado'),
(30, 'Registrar categoría de producto');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `privilegio_rol`
--

CREATE TABLE IF NOT EXISTS `privilegio_rol` (
  `id_rol` int(3) NOT NULL,
  `id_privilegio` int(3) NOT NULL,
  PRIMARY KEY (`id_rol`,`id_privilegio`),
  KEY `privilegiorolprivilegio` (`id_privilegio`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `producto`
--

CREATE TABLE IF NOT EXISTS `producto` (
  `id_producto` int(4) NOT NULL AUTO_INCREMENT,
  `nombre_producto` varchar(25) COLLATE utf8_spanish2_ci NOT NULL,
  `descripcion` varchar(20) COLLATE utf8_spanish2_ci NOT NULL,
  `costo` decimal(6,2) NOT NULL,
  `existencia` int(3) NOT NULL,
  `presentacion` varchar(10) COLLATE utf8_spanish2_ci NOT NULL,
  `imagen` varchar(200) COLLATE utf8_spanish2_ci NOT NULL,
  `id_categoria` int(4) NOT NULL,
  `estado_producto` varchar(20) COLLATE utf8_spanish2_ci NOT NULL,
  PRIMARY KEY (`id_producto`),
  KEY `productocategoria` (`id_categoria`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci AUTO_INCREMENT=85 ;

--
-- Volcado de datos para la tabla `producto`
--

INSERT INTO `producto` (`id_producto`, `nombre_producto`, `descripcion`, `costo`, `existencia`, `presentacion`, `imagen`, `id_categoria`, `estado_producto`) VALUES
(1, 'Yogurt batido', 'Natural', '19.00', 9, '500 ml', 'http://imagizer.imageshack.us/v2/320x240q90/921/aJ2KA8.jpg', 1, 'activo'),
(2, 'Yogurt batido', 'Fresa', '15.00', 52, '500 ml', 'http://imageshack.com/a/img924/1536/fh4su0.jpg', 1, 'activo'),
(3, 'Yogurt batido', 'Manzana', '15.00', 34, '500 ml', 'http://imageshack.com/a/img922/4231/vNXmzN.jpg', 1, 'activo'),
(4, 'Yogurt batido', 'Durazno', '15.00', 31, '500 ml', 'http://imageshack.com/a/img922/126/6xvg3j.jpg', 1, 'activo'),
(5, 'Yogurt batido', 'Mango', '15.00', 69, '500 ml', 'http://imageshack.com/a/img924/7223/GC1tiy.jpg', 1, 'activo'),
(6, 'Yogurt batido', 'Nuez', '15.00', 23, '500 ml', 'http://imageshack.com/a/img921/6628/HAfSQ5.jpg', 1, 'activo'),
(7, 'Yogurt batido', 'Natural', '5.00', 0, '200 ml', 'http://imageshack.com/a/img921/327/0AhbOx.jpg', 1, 'activo'),
(8, 'Yogurt batido', 'Fresa', '5.00', 2, '200 ml', 'http://imageshack.com/a/img922/7069/OwLWQx.jpg', 1, 'activo'),
(9, 'Yogurt batido', 'Manzana', '5.00', 17, '200 ml', 'http://imageshack.com/a/img921/5316/CdjAv6.jpg', 1, 'activo'),
(10, 'Yogurt batido', 'Durazno', '5.00', 27, '200 ml', 'http://imageshack.com/a/img923/2189/F2vS7v.jpg', 1, 'activo'),
(11, 'Yogurt batido', 'Mango', '5.00', 121, '200 ml', 'http://imageshack.com/a/img922/2941/kiKCqy.jpg', 1, 'activo'),
(12, 'Yogurt batido', 'Nuez', '5.00', 15, '200 ml', 'http://imageshack.com/a/img922/8942/RZmej6.jpg', 1, 'activo'),
(13, 'Rompope', 'Vainilla', '50.00', 22, '500 ml', 'http://imageshack.com/a/img924/637/Gz9ykr.jpg', 1, 'activo'),
(14, 'Chongos zamoranos', 'Natural', '50.00', 35, '600 g', 'http://imageshack.com/a/img922/2687/zXfJmu.jpg', 1, 'activo'),
(15, 'Chongos zamoranos', 'Natural', '25.00', 30, '300 g', 'http://imageshack.com/a/img924/8704/6Iilfc.jpg', 1, 'activo'),
(16, 'Dulce de leche', 'Natural', '40.00', 35, '500 ml', 'http://imageshack.com/a/img924/4631/Z4ebEw.jpg', 1, 'activo'),
(17, 'Dulce de leche', 'Vainilla', '40.00', 9, '500 ml', 'http://imageshack.com/a/img924/9554/8X85BD.jpg', 1, 'activo'),
(18, 'Dulce de leche', 'Envinada', '40.00', 35, '500 ml', 'http://imageshack.com/a/img923/8710/zy99K4.jpg', 1, 'activo'),
(19, 'Nieve', 'Mantecado', '30.00', 22, '500 ml', 'http://imageshack.com/a/img921/2913/lgipem.jpg', 1, 'activo'),
(20, 'Nieve', 'Chocolate', '30.00', 5, '500 ml', 'http://imageshack.com/a/img924/310/f68Avf.jpg', 1, 'activo'),
(21, 'Nieve', 'Vainilla', '30.00', 17, '500 ml', 'http://imageshack.com/a/img922/5723/uWWeX1.jpg', 1, 'activo'),
(22, 'Nieve', 'Melón', '30.00', 22, '500 ml', 'http://imageshack.com/a/img924/3435/DvMx0L.jpg', 1, 'activo'),
(23, 'Nieve', 'Fresa', '30.00', 20, '500 ml', 'http://imageshack.com/a/img921/277/FZnc4a.png', 1, 'activo'),
(24, 'Nieve', 'Mango', '30.00', 2, '500 ml', 'http://imageshack.com/a/img923/7927/CkybAJ.jpg', 1, 'activo'),
(25, 'Nieve', 'Guayaba', '30.00', 33, '500 ml', 'http://imageshack.com/a/img922/3333/seilHG.jpg', 1, 'activo'),
(26, 'Nieve', 'Nuez', '30.00', 33, '500 ml', 'http://imageshack.com/a/img924/8687/Uo4g5E.jpg', 1, 'activo'),
(27, 'Nieve', 'Chocolate', '10.00', 5, '200 ml', 'http://imageshack.com/a/img924/2808/irdh6o.jpg', 1, 'activo'),
(28, 'Nieve', 'Vainilla', '10.00', 30, '200 ml', 'http://imageshack.com/a/img924/3449/E1KO9K.jpg', 1, 'activo'),
(29, 'Nieve', 'Melón', '10.00', 5, '200 ml', 'http://imageshack.com/a/img924/5027/mWewrf.jpg', 1, 'activo'),
(30, 'Nieve', 'Fresa', '10.00', 2, '200 ml', 'http://imageshack.com/a/img922/7362/bqnJtc.png', 1, 'activo'),
(31, 'Nieve', 'Mango', '10.00', 12, '200 ml', 'http://imageshack.com/a/img924/553/a61Qz4.jpg', 1, 'activo'),
(32, 'Nieve', 'Guayaba', '10.00', 37, '200 ml', 'http://imageshack.com/a/img924/6363/CpRON5.jpg', 1, 'activo'),
(33, 'Nieve', 'Nuez', '10.00', 38, '200 ml', 'http://imageshack.com/a/img921/4216/PH6Hat.jpg', 1, 'activo'),
(34, 'Queso Panela', 'Natural', '9.50', 17, '50g', 'http://imageshack.com/a/img921/4265/EQda8c.jpg', 1, 'activo'),
(35, 'Queso Oaxaca', 'Natural', '95.00', 38, '500 g', 'http://imageshack.com/a/img921/4405/OqQ73I.jpg', 1, 'activo'),
(36, 'Queso Manchego', 'Natural', '120.00', 3, '500 g', 'http://imageshack.com/a/img922/4559/ZMZQno.jpg', 1, 'activo'),
(37, 'Queso Chihuahua', 'Natural', '120.00', 20, '500 g', 'http://imageshack.com/a/img922/6817/3fdXhx.jpg', 1, 'activo'),
(38, 'Queso Cotija', 'Natural', '120.00', 11, '500 g', 'http://imageshack.com/a/img921/8039/ZCqYVu.jpg', 1, 'activo'),
(39, 'Queso Gouda', 'Natural', '120.00', 39, '500 g', 'http://imageshack.com/a/img924/855/FfakS9.jpg', 1, 'activo'),
(40, 'Queso Doble crema', 'Natural', '110.00', 40, '500 g', 'http://imageshack.com/a/img922/5241/F2LqWY.jpg', 1, 'activo'),
(41, 'Queso Ranchero', 'Natural', '100.00', 32, '500 g', 'http://imageshack.com/a/img921/3869/l5VgSy.png', 1, 'activo'),
(42, 'Requesón', 'Natural', '80.00', 21, '500 g', 'http://imageshack.com/a/img922/5051/VLFazV.jpg', 2, 'activo'),
(43, 'Sausage', 'Queso & Jalapeño', '25.00', 18, 'Paquete de', 'http://imageshack.com/a/img924/2433/N31Su1.jpg', 2, 'activo'),
(44, 'Bratwurst', 'Natural', '25.00', 10, 'Paquete de', 'http://imageshack.com/a/img924/7824/NlxhiR.jpg', 2, 'activo'),
(45, 'Chorizo', 'Argentino', '25.00', 30, 'Paquete de', 'http://imageshack.com/a/img924/7095/jlj7ER.jpg', 2, 'activo'),
(46, 'Chorizo', 'Español', '25.00', 29, 'Paquete de', 'http://imageshack.com/a/img924/4582/hkTr0F.jpg', 2, 'activo'),
(47, 'Chorizo', 'Mexicano', '25.00', 32, 'Paquete de', 'http://imageshack.com/a/img923/5530/qT3dCr.jpg', 2, 'activo'),
(48, 'Jamón', 'Ahumado', '35.00', 33, '200 g', 'http://imageshack.com/a/img923/9450/R970j2.jpg', 2, 'activo'),
(49, 'Jamón', 'Natural', '36.00', 25, '200 g', 'http://imageshack.com/a/img923/5569/cPBwEc.jpg', 2, 'activo'),
(50, 'Jamón', 'Especias ', '37.00', 22, '200 g', 'http://imageshack.com/a/img924/7458/rXfdTe.jpg', 2, 'activo'),
(51, 'Tocino', 'Ahumado', '40.00', 26, '250 g', 'http://imageshack.com/a/img924/180/xnBKSf.jpg', 2, 'activo'),
(52, 'Chistorra', 'Natural', '30.00', 29, '200 g', 'http://imageshack.com/a/img923/7767/QWeEMq.jpg', 2, 'activo'),
(53, 'Salchicha hotdog', 'Res', '50.00', 35, '200 g', 'http://imageshack.com/a/img924/9583/17bF6R.jpg', 2, 'activo'),
(54, 'Mermelada', 'Fresa', '35.00', 44, '500 g', 'http://imageshack.com/a/img922/1690/Kvz6Li.jpg', 3, 'activo'),
(55, 'Mermelada', 'Naranja', '35.00', 28, '500 g', 'http://imageshack.com/a/img924/4641/FXYJED.jpg', 3, 'activo'),
(56, 'Mermelada', 'Piña', '35.00', 27, '500 g', 'http://imageshack.com/a/img923/1462/J6YvzC.jpg', 3, 'activo'),
(57, 'Rollo de guayaba', 'Naranja', '15.00', 6, '200 g', 'http://imageshack.com/a/img922/153/C0LSuD.jpg', 3, 'activo'),
(58, 'Fresas cubiertas de choco', 'Natural', '15.00', 27, '200 g', 'http://imageshack.com/a/img921/2489/Mb6uL7.jpg', 3, 'activo'),
(59, 'Piñas con chile', 'Natural', '15.00', 35, '200 g', 'http://imageshack.com/a/img921/6866/pOv3ly.jpg', 3, 'activo'),
(60, 'Verduras en escabeche', 'Natural', '35.00', 35, '500 g', 'http://imageshack.com/a/img923/4653/4hzALb.jpg', 3, 'activo'),
(61, 'Chiles en escabeche', 'Natural', '25.00', 6, '500 g', 'http://imageshack.com/a/img924/329/CMQOiO.jpg', 3, 'activo'),
(62, 'Ensalada', 'César con pollo', '30.00', 24, '450 g', 'http://imageshack.com/a/img922/1015/e0pfP1.jpg', 3, 'activo'),
(63, 'Ensalada', 'Pechuga de pavo', '30.00', 31, '450 g', 'http://imageshack.com/a/img924/9119/EnAUYn.jpg', 3, 'activo'),
(64, 'Sopa', 'Vegetales', '30.00', 7, '200 ml', 'http://imageshack.com/a/img921/8587/ekQUCH.jpg', 4, 'activo'),
(65, 'Sopa', 'Pollo', '30.00', 29, '200 ml', 'http://imageshack.com/a/img923/7197/A3cofi.jpg', 4, 'activo'),
(66, 'Muffins', 'Avena', '20.00', 34, '1 pieza', 'http://imageshack.com/a/img923/7671/ix2zBe.jpg', 4, 'activo'),
(67, 'Muffins', 'Chocochips', '20.00', 14, '1 pieza', 'http://imageshack.com/a/img923/5792/wlMbH1.jpg', 4, 'activo'),
(68, 'Muffins', 'Banana', '20.00', 9, '1 pieza', 'http://imageshack.com/a/img924/9282/DGpMmO.jpg', 4, 'activo'),
(69, 'Muffins', 'Chocolate', '20.00', 19, '1 pieza', 'http://imageshack.com/a/img924/9082/DH0Rwu.jpg', 4, 'activo'),
(70, 'Panqué', 'Chocolate', '20.00', 16, '1 pieza', 'http://imageshack.com/a/img924/4495/Eet3gD.jpg', 4, 'activo'),
(71, 'Panqué', 'Limón', '20.00', 6, '1 pieza', 'http://imageshack.com/a/img921/8840/cu5O2T.jpg', 4, 'activo'),
(72, 'Panqué', 'Naranja', '20.00', 30, '1 pieza', 'http://imageshack.com/a/img922/2277/RWsXY9.jpg', 4, 'activo'),
(73, 'Roles de Canela', 'Glaseado', '20.00', 2, 'Paquete de', 'http://imageshack.com/a/img922/5097/gxmD0o.jpg', 4, 'activo'),
(74, 'Pasta', 'Natural', '20.00', 28, '700 g', 'http://imageshack.com/a/img924/9301/6uY67M.jpg', 4, 'activo'),
(75, 'Pasta', 'Albahaca', '20.00', 7, '700 g', 'http://imageshack.com/a/img923/489/Iio9jC.jpg', 4, 'activo'),
(84, 'Prueba de Juan', 'Prueba', '20.53', 0, 'Prueba', 'google.com', 1, 'activo');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `roles`
--

CREATE TABLE IF NOT EXISTS `roles` (
  `id_rol` int(3) NOT NULL AUTO_INCREMENT,
  `nombre_rol` varchar(30) COLLATE utf8_spanish2_ci NOT NULL,
  PRIMARY KEY (`id_rol`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci AUTO_INCREMENT=5 ;

--
-- Volcado de datos para la tabla `roles`
--

INSERT INTO `roles` (`id_rol`, `nombre_rol`) VALUES
(1, 'Administrador'),
(2, 'Gerente'),
(3, 'Vendedor'),
(4, 'Cliente');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rol_usuario`
--

CREATE TABLE IF NOT EXISTS `rol_usuario` (
  `id_rol` int(3) NOT NULL,
  `id_usuario` int(4) NOT NULL,
  PRIMARY KEY (`id_rol`,`id_usuario`),
  KEY `rolusuariousuario` (`id_usuario`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `rol_usuario`
--

INSERT INTO `rol_usuario` (`id_rol`, `id_usuario`) VALUES
(1, 1),
(3, 2),
(3, 3),
(3, 4),
(2, 5),
(4, 6),
(4, 7),
(2, 8),
(2, 9),
(4, 10),
(2, 11),
(4, 12),
(4, 13),
(4, 14),
(4, 15),
(2, 16),
(3, 17),
(2, 18),
(2, 19),
(2, 20),
(4, 21),
(2, 22),
(3, 23),
(3, 24),
(2, 25),
(2, 26),
(4, 27),
(2, 28),
(4, 29),
(3, 30),
(3, 31),
(2, 32),
(2, 33),
(3, 34),
(2, 35),
(2, 36),
(3, 37),
(3, 38),
(3, 39),
(3, 40),
(4, 40),
(4, 41),
(2, 42),
(3, 43),
(4, 44),
(3, 45),
(4, 46),
(2, 47),
(4, 48),
(4, 49),
(2, 50),
(4, 52),
(4, 53),
(4, 54),
(3, 55),
(4, 55),
(4, 56),
(3, 57),
(4, 57),
(4, 58),
(4, 59),
(4, 60);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_pago`
--

CREATE TABLE IF NOT EXISTS `tipo_pago` (
  `id_pago` int(4) NOT NULL AUTO_INCREMENT,
  `nombre_pago` varchar(20) COLLATE utf8_spanish2_ci NOT NULL,
  PRIMARY KEY (`id_pago`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci AUTO_INCREMENT=3 ;

--
-- Volcado de datos para la tabla `tipo_pago`
--

INSERT INTO `tipo_pago` (`id_pago`, `nombre_pago`) VALUES
(1, 'Efectivo'),
(2, 'Tarjeta');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE IF NOT EXISTS `usuario` (
  `id_usuario` int(4) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  `apellido` varchar(30) COLLATE utf8_spanish2_ci NOT NULL,
  `correo` varchar(30) COLLATE utf8_spanish2_ci NOT NULL,
  `telefono` varchar(20) COLLATE utf8_spanish2_ci NOT NULL,
  `contrasena` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  `estado` varchar(20) COLLATE utf8_spanish2_ci NOT NULL,
  PRIMARY KEY (`id_usuario`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci AUTO_INCREMENT=61 ;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`id_usuario`, `nombre`, `apellido`, `correo`, `telefono`, `contrasena`, `estado`) VALUES
(1, 'Rubén', 'Zárraga', 'admin@gmail.com', '4421111144', '9c87400128d408cdcda0e4b3ff0e66fa', 'activo'),
(2, 'Jorge', 'Rondón', 'test@gmail.com', '4422222211', '9c87400128d408cdcda0e4b3ff0e66fa', 'inactivo'),
(3, 'Armando', 'Hokolesqua Guapo', 'armando@gmail.com', '4422531636', '9c87400128d408cdcda0e4b3ff0e66fa', 'activo'),
(4, 'Artemio', 'José Gomez', 'artemiojose@gmail.com', '4428923657', '9c87400128d408cdcda0e4b3ff0e66fa', 'inactivo'),
(5, 'Artemio', 'Paco Mingo', 'mingobingo@gmail.com', '4432758868', '9c87400128d408cdcda0e4b3ff0e66fa', 'inactivo'),
(6, 'Bartolom', 'Teobaldo Belmonte', 'belmontemonte@gmail.com', '4435315676', '9c87400128d408cdcda0e4b3ff0e66fa', 'activo'),
(7, 'Benigna', 'Mariana Zubizarreta', 'zubirrrrrrr@gmail.com', '4447460514', '9c87400128d408cdcda0e4b3ff0e66fa', 'activo'),
(8, 'Catalina', 'Clementina loca', 'ibezguitarras@gmail.com', '4437233282', '9c87400128d408cdcda0e4b3ff0e66fa', 'activo'),
(9, 'Catalina', 'Emelina Abel', 'Abelinademiamor@gmail.com', '4428284454', '9c87400128d408cdcda0e4b3ff0e66fa', 'activo'),
(10, 'Cecilia', 'Paola Suárez', 'suarezceci@gmail.com', '4433398070', '9c87400128d408cdcda0e4b3ff0e66fa', 'activo'),
(11, 'Chimo', 'Duilio Castellano', 'duicastellanos@gmail.com', '4425088444', '9c87400128d408cdcda0e4b3ff0e66fa', 'activo'),
(12, 'Clara', 'Rosalinda Berm_dez', 'rosalindanovela@gmail.com', '4432119666', '9c87400128d408cdcda0e4b3ff0e66fa', 'activo'),
(13, 'Clemente', 'Ahtahkakoop Velazquez', 'zdgfsfsfds_velazquez@gmail.com', '4439150888', '9c87400128d408cdcda0e4b3ff0e66fa', 'activo'),
(14, 'Constanza', 'Ígueda Valencia', 'valencia.a@gmail.com', '4450017322', '9c87400128d408cdcda0e4b3ff0e66fa', 'activo'),
(15, 'Eladio', 'Anacleto Zavala', 'eladio_zaval@gmail.com', '4430202060', '9c87400128d408cdcda0e4b3ff0e66fa', 'activo'),
(16, 'Elba', 'Sol Macías', 'elbasol@gmail.com', '4421892434', '9c87400128d408cdcda0e4b3ff0e66fa', 'activo'),
(17, 'Eligio', 'Heliodoro Duarte', 'heliduarte@gmail.com', '4421253232', '9c87400128d408cdcda0e4b3ff0e66fa', 'activo'),
(18, 'Elisa', 'Armida Salinas', 'salinas_salada@gmail.com', '4446182110', '9c87400128d408cdcda0e4b3ff0e66fa', 'activo'),
(19, 'Elisa', 'Salomé Tapia', 'tapiadelolvido@gmail.com', '4439790090', '9c87400128d408cdcda0e4b3ff0e66fa', 'inactivo'),
(20, 'Elodia', 'Sancha Medina', 'sancha_med@gmail.com', '4425727646', '9c87400128d408cdcda0e4b3ff0e66fa', 'activo'),
(21, 'Emiliano ', 'Heliodoro Romero', 'Romerojaja@gmail.com', '4427645252', '9c87400128d408cdcda0e4b3ff0e66fa', 'activo'),
(22, 'Emilio', 'Xuan Salcedo', 'xuanju@gmail.com', '4440429292', '9c87400128d408cdcda0e4b3ff0e66fa', 'activo'),
(23, 'Esperanza ', 'Nydia Maradona', 'esperanzaparaelcorazon@gmail.c', '4441068494', '9c87400128d408cdcda0e4b3ff0e66fa', 'activo'),
(24, 'Germán ', 'Adrián Abel', 'abelcruiz@gmail.com', '4451934928', '9c87400128d408cdcda0e4b3ff0e66fa', 'activo'),
(25, 'Ivette ', 'Nuria Escórcega', 'ivettenuria@gmail.com', '4429562858', '9c87400128d408cdcda0e4b3ff0e66fa', 'activo'),
(26, 'Jenaro ', 'Teófilo Vásquez', 'jenaro_jenarito@gmail.com', '4441707696', '9c87400128d408cdcda0e4b3ff0e66fa', 'activo'),
(27, 'Jonatan', 'Ale Durante', 'durantelaguerra@gmail.com', '4434037272', '9c87400128d408cdcda0e4b3ff0e66fa', 'activo'),
(28, 'Jonatan', 'Régulo Castro', 'regulolasreglasjaja@gmail.com', '4442986100', '9c87400128d408cdcda0e4b3ff0e66fa', 'activo'),
(29, 'José', 'Primitivo Hierro', 'primitivo_man@gmail.com', '4426366848', '9c87400128d408cdcda0e4b3ff0e66fa', 'activo'),
(30, 'Sir Franco', 'Valentino', 'francoval@gmail.com', '422222222', '9c87400128d408cdcda0e4b3ff0e66fa', 'activo'),
(31, 'Manuela', 'Calfuray Colón', 'colon_cristobal_calfuray@gmail', '4452574130', '9c87400128d408cdcda0e4b3ff0e66fa', 'activo'),
(32, 'Margarita', 'Sabina Durante', 'sabinalabrujis@gmail.com', '4444903706', '9c87400128d408cdcda0e4b3ff0e66fa', 'activo'),
(33, 'Maximino', 'Marcio Valdez', 'maximinodelcielo@gmail.com', '4436594080', '9c87400128d408cdcda0e4b3ff0e66fa', 'activo'),
(34, 'Nicodemo', 'Tristán Vela', 'veladefuego@gmail.com', '4449378120', '9c87400128d408cdcda0e4b3ff0e66fa', 'activo'),
(35, 'Nicolao', 'Cruz Alfaro', 'alfaro_alaluz@gmail.com', '4437872484', '9c87400128d408cdcda0e4b3ff0e66fa', 'inactivo'),
(36, 'Octavia', 'africa Gallo', 'gallo_gallina@gmail.com', '4451295726', '9c87400128d408cdcda0e4b3ff0e66fa', 'inactivo'),
(37, 'Raul', 'Eleuterio Campo', 'eleuterius99@gmail.com', '4423810040', '9c87400128d408cdcda0e4b3ff0e66fa', 'activo'),
(38, 'Rodrigo', 'Antiman Abano', 'abano_arbano@gmail.com', '4446821312', '9c87400128d408cdcda0e4b3ff0e66fa', 'activo'),
(39, 'Pulga', 'Rodríguez', 'puga_lapulga@gmail.com', '1234567891', '9c87400128d408cdcda0e4b3ff0e66fa', 'activo'),
(40, 'Harmon', 'Hall', 'basurtir@gmail.com', '424231242342', '9c87400128d408cdcda0e4b3ff0e66fa', 'inactivo'),
(41, 'Samuel', 'Elpidio Aiza', 'elpidio_un_milagro@gmail.com', '4431480464', '9c87400128d408cdcda0e4b3ff0e66fa', 'activo'),
(42, 'Santos', 'Osvaldo Chavarrias', 'osvaldodelossantos@gmail.com', '4445542908', '9c87400128d408cdcda0e4b3ff0e66fa', 'inactivo'),
(43, 'Saqui', 'Chus Torres', 'saquiunmoqui@gmail.com', '4443625302', '9c87400128d408cdcda0e4b3ff0e66fa', 'inactivo'),
(44, 'Shikoba', 'Heraclio Menendez', 'shikoba_nokoba@gmail.com', '4450656524', '9c87400128d408cdcda0e4b3ff0e66fa', 'activo'),
(45, 'Soledad', 'Hernández', 'soledadytristeza@gmail.com', '3234124542', '9c87400128d408cdcda0e4b3ff0e66fa', 'activo'),
(46, 'Tecla', 'Fortunata Capello', 'lafortunatasoy_yo@gmail.com', '4430841262', '9c87400128d408cdcda0e4b3ff0e66fa', 'activo'),
(47, 'Teófila Carla', 'San Nicolas', 'Carla@gmail.com', '4423170838', '9c87400128d408cdcda0e4b3ff0e66fa', 'activo'),
(48, 'Yessica', 'Marisela Bustos', 'maquinadefuego99@gmail.com', '4424449242', '9c87400128d408cdcda0e4b3ff0e66fa', 'activo'),
(49, 'Yessica', 'Paloma Santana', 'santana_hola@gmail.com', '4442346898', '9c87400128d408cdcda0e4b3ff0e66fa', 'activo'),
(50, 'Zoraida', 'Marina Sierras', 'zoraida@gmail.com', '4435954878', '9c87400128d408cdcda0e4b3ff0e66fa', 'inactivo'),
(52, 'Franco', 'Valencia', 'franco@gmail.com', '444444444', '2425d38c49253de6987da872ebcbb62f', 'activo'),
(53, 'Tyler', 'Durden', 'durden@gmail.com', '333333333', '2425d38c49253de6987da872ebcbb62f', 'activo'),
(54, 'Jack', 'Sparrow', 'jack@gmail.com', '55555555', '9c87400128d408cdcda0e4b3ff0e66fa', 'activo'),
(55, 'Franco', 'Valencia', 'franco2@gmail.com', '555555555', '9c87400128d408cdcda0e4b3ff0e66fa', 'activo'),
(56, 'Regina', 'Balderas', 'regina@itesm.mx', '55555555', '9c87400128d408cdcda0e4b3ff0e66fa', 'activo'),
(57, 'Juan', 'Guerrero', 'juans_gro@hotmail.com', '4423334435', '9c87400128d408cdcda0e4b3ff0e66fa', 'activo'),
(58, 'Martin', 'Molinero', 'A01204818@hotmail.com', '4421578527', '3d12a891752d9b1c2a1c919469eaf6f1', 'activo'),
(59, 'Alejandra', 'Cárdenas', 'alejandra.cardenas@mabe.com.mx', '4422812564', '9c87400128d408cdcda0e4b3ff0e66fa', 'activo'),
(60, 'Ochoa', 'Ochoa', 'ochos@gmail.com', '4421111144', '9c87400128d408cdcda0e4b3ff0e66fa', 'activo');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario_aparta_producto`
--

CREATE TABLE IF NOT EXISTS `usuario_aparta_producto` (
  `id_apartado` int(4) NOT NULL AUTO_INCREMENT,
  `id_usuario` int(4) NOT NULL,
  `id_producto` int(4) NOT NULL,
  `fecha_apartado` datetime NOT NULL,
  `cantidad_apartado` int(3) NOT NULL,
  `estado_apartado` varchar(20) COLLATE utf8_spanish2_ci NOT NULL,
  PRIMARY KEY (`id_apartado`,`id_usuario`,`id_producto`,`fecha_apartado`),
  KEY `producto_apartado` (`id_producto`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci AUTO_INCREMENT=156 ;

--
-- Volcado de datos para la tabla `usuario_aparta_producto`
--

INSERT INTO `usuario_aparta_producto` (`id_apartado`, `id_usuario`, `id_producto`, `fecha_apartado`, `cantidad_apartado`, `estado_apartado`) VALUES
(1, 7, 19, '2016-01-01 10:00:00', 4, 'expirado'),
(2, 18, 69, '2016-01-02 10:05:00', 5, 'expirado'),
(3, 1, 19, '2016-01-03 10:10:00', 4, 'expirado'),
(4, 3, 67, '2016-01-04 10:15:00', 4, 'expirado'),
(5, 10, 3, '2016-01-05 10:20:00', 1, 'expirado'),
(6, 26, 50, '2016-01-06 10:25:00', 4, 'expirado'),
(7, 7, 23, '2016-01-07 10:30:00', 3, 'expirado'),
(8, 48, 54, '2016-01-08 10:35:00', 1, 'expirado'),
(9, 3, 51, '2016-01-09 10:40:00', 3, 'expirado'),
(10, 4, 4, '2016-01-10 10:45:00', 1, 'expirado'),
(11, 10, 4, '2016-01-11 10:50:00', 3, 'expirado'),
(12, 11, 6, '2016-01-12 10:55:00', 5, 'expirado'),
(13, 34, 38, '2016-01-13 11:00:00', 1, 'expirado'),
(14, 17, 74, '2016-01-14 11:05:00', 4, 'expirado'),
(15, 17, 70, '2016-01-15 11:10:00', 2, 'expirado'),
(16, 48, 26, '2016-01-16 11:15:00', 1, 'expirado'),
(17, 26, 49, '2016-01-17 11:20:00', 3, 'expirado'),
(18, 26, 13, '2016-01-18 11:25:00', 2, 'expirado'),
(19, 32, 75, '2016-01-19 11:30:00', 2, 'expirado'),
(20, 1, 27, '2016-01-01 10:00:00', 3, 'expirado'),
(21, 39, 54, '2016-01-01 10:05:00', 1, 'expirado'),
(22, 10, 45, '2016-01-01 10:10:00', 3, 'expirado'),
(23, 29, 11, '2016-01-01 10:15:00', 1, 'expirado'),
(24, 34, 28, '2016-01-01 10:20:00', 2, 'expirado'),
(25, 41, 19, '2016-01-01 10:25:00', 5, 'expirado'),
(26, 21, 11, '2016-01-01 10:30:00', 1, 'expirado'),
(27, 31, 59, '2016-01-01 10:35:00', 2, 'expirado'),
(28, 32, 31, '2016-01-01 10:40:00', 4, 'expirado'),
(29, 39, 40, '2016-01-01 10:45:00', 5, 'expirado'),
(30, 4, 28, '2016-01-01 10:50:00', 1, 'expirado'),
(31, 41, 30, '2016-01-01 10:55:00', 1, 'expirado'),
(32, 2, 56, '2016-01-01 11:00:00', 1, 'expirado'),
(33, 43, 17, '2016-01-01 11:05:00', 3, 'expirado'),
(34, 46, 17, '2016-01-01 11:10:00', 1, 'expirado'),
(35, 41, 28, '2016-01-01 11:15:00', 3, 'expirado'),
(36, 1, 70, '2016-01-01 11:20:00', 2, 'expirado'),
(37, 18, 9, '2016-01-01 11:25:00', 2, 'expirado'),
(38, 28, 36, '2016-01-01 11:30:00', 3, 'expirado'),
(39, 43, 34, '2016-01-01 10:00:00', 3, 'expirado'),
(40, 25, 12, '2016-01-01 10:05:00', 1, 'expirado'),
(41, 23, 67, '2016-01-01 10:10:00', 2, 'expirado'),
(42, 22, 70, '2016-01-01 10:15:00', 5, 'expirado'),
(43, 39, 57, '2016-01-01 10:20:00', 2, 'expirado'),
(44, 9, 4, '2016-01-01 10:25:00', 4, 'expirado'),
(45, 40, 4, '2016-01-01 10:30:00', 1, 'expirado'),
(46, 18, 11, '2016-01-01 10:35:00', 4, 'expirado'),
(47, 27, 16, '2016-01-01 10:40:00', 2, 'expirado'),
(48, 9, 41, '2016-01-01 10:45:00', 4, 'expirado'),
(49, 5, 41, '2016-01-01 10:50:00', 3, 'expirado'),
(50, 22, 15, '2016-01-01 10:55:00', 2, 'expirado'),
(51, 9, 67, '2016-01-01 11:00:00', 4, 'expirado'),
(52, 46, 34, '2016-01-01 11:05:00', 4, 'expirado'),
(53, 50, 59, '2016-01-01 11:10:00', 1, 'expirado'),
(54, 38, 30, '2016-01-01 11:15:00', 2, 'expirado'),
(55, 4, 1, '2016-01-01 11:20:00', 4, 'expirado'),
(56, 38, 58, '2016-01-01 11:25:00', 3, 'expirado'),
(57, 8, 56, '2016-01-01 11:30:00', 3, 'expirado'),
(58, 2, 72, '2016-02-27 10:00:00', 1, 'expirado'),
(59, 46, 3, '2016-02-28 10:05:00', 5, 'expirado'),
(60, 51, 14, '2016-02-29 10:10:00', 3, 'expirado'),
(61, 12, 38, '2016-03-01 10:15:00', 1, 'expirado'),
(62, 37, 58, '2016-03-02 10:20:00', 1, 'expirado'),
(63, 22, 68, '2016-03-03 10:25:00', 3, 'expirado'),
(64, 11, 45, '2016-03-04 10:30:00', 4, 'expirado'),
(65, 46, 69, '2016-03-05 10:35:00', 1, 'expirado'),
(66, 9, 22, '2016-03-06 10:40:00', 3, 'expirado'),
(67, 42, 61, '2016-03-07 10:45:00', 1, 'expirado'),
(68, 24, 14, '2016-03-08 10:50:00', 2, 'expirado'),
(69, 21, 16, '2016-03-09 10:55:00', 4, 'expirado'),
(70, 45, 33, '2016-03-10 11:00:00', 1, 'expirado'),
(71, 1, 1, '2016-04-04 22:44:12', 2, 'expirado'),
(72, 1, 1, '2016-04-04 23:01:08', 2, 'expirado'),
(73, 1, 2, '2016-04-05 19:22:02', 1, 'expirado'),
(74, 1, 2, '2016-04-05 19:47:41', 1, 'expirado'),
(75, 1, 1, '2016-04-10 21:20:38', 1, 'expirado'),
(76, 1, 1, '2016-04-15 22:29:27', 1, 'expirado'),
(77, 1, 2, '2016-04-15 22:36:55', 1, 'expirado'),
(78, 1, 2, '2016-04-15 23:06:37', 1, 'expirado'),
(79, 1, 64, '2016-04-15 23:34:36', 1, 'expirado'),
(80, 1, 54, '2016-04-15 23:35:33', 1, 'expirado'),
(81, 1, 54, '2016-04-15 23:37:54', 1, 'expirado'),
(82, 1, 54, '2016-04-15 23:41:05', 1, 'expirado'),
(83, 1, 3, '2016-04-15 23:41:17', 1, 'expirado'),
(84, 1, 2, '2016-04-15 23:43:24', 1, 'expirado'),
(85, 1, 1, '2016-04-16 00:52:34', 1, 'expirado'),
(86, 1, 64, '2016-04-16 20:48:25', 1, 'expirado'),
(87, 1, 64, '2016-04-16 20:48:36', 1, 'expirado'),
(88, 1, 64, '2016-04-16 20:49:05', 1, 'expirado'),
(89, 1, 62, '2016-04-16 20:50:13', 1, 'expirado'),
(90, 1, 62, '2016-04-16 20:50:28', 1, 'expirado'),
(91, 1, 3, '2016-04-18 18:13:50', 1, 'expirado'),
(92, 1, 1, '2016-04-18 18:14:49', 1, 'expirado'),
(93, 1, 1, '2016-04-18 18:15:30', 1, 'expirado'),
(94, 1, 1, '2016-04-18 21:56:15', 1, 'expirado'),
(95, 1, 2, '2016-04-19 15:29:00', 1, 'expirado'),
(96, 1, 2, '2016-04-19 15:32:06', 1, 'expirado'),
(97, 1, 2, '2016-04-19 15:33:43', 1, 'expirado'),
(98, 1, 2, '2016-04-19 15:40:09', 1, 'expirado'),
(99, 1, 5, '2016-04-19 17:20:47', 5, 'expirado'),
(100, 1, 5, '2016-04-19 17:21:00', 5, 'expirado'),
(101, 1, 2, '2016-04-19 17:21:56', 5, 'expirado'),
(102, 1, 34, '2016-04-19 17:22:35', 18, 'expirado'),
(103, 1, 6, '2016-04-19 17:28:21', 13, 'expirado'),
(104, 1, 3, '2016-04-19 18:00:09', 1, 'expirado'),
(105, 1, 2, '2016-04-20 13:48:28', 1, 'expirado'),
(106, 1, 1, '2016-04-21 21:26:28', 1, 'expirado'),
(107, 1, 3, '2016-04-22 14:48:44', 1, 'expirado'),
(108, 1, 3, '2016-04-22 14:53:42', 1, 'expirado'),
(109, 1, 3, '2016-04-22 14:55:07', 1, 'expirado'),
(110, 1, 3, '2016-04-22 15:00:04', 1, 'expirado'),
(111, 1, 1, '2016-04-22 15:03:42', 1, 'expirado'),
(112, 1, 1, '2016-04-22 15:04:12', 1, 'expirado'),
(113, 1, 1, '2016-04-22 15:05:28', 1, 'expirado'),
(114, 1, 1, '2016-04-22 15:11:02', 1, 'expirado'),
(115, 1, 1, '2016-04-22 15:14:09', 1, 'expirado'),
(116, 1, 2, '2016-04-22 15:23:01', 1, 'expirado'),
(117, 52, 4, '2016-04-23 18:31:24', 2, 'expirado'),
(118, 52, 4, '2016-04-23 18:40:09', 1, 'expirado'),
(119, 1, 2, '2016-04-25 16:29:30', 1, 'expirado'),
(120, 2, 2, '2016-04-25 16:29:32', 1, 'expirado'),
(121, 2, 23, '2016-04-25 16:31:03', 3, 'expirado'),
(122, 52, 2, '2016-04-25 16:31:21', 1, 'expirado'),
(123, 2, 24, '2016-04-25 16:31:34', 3, 'expirado'),
(124, 2, 1, '2016-04-25 16:35:19', 20, 'expirado'),
(125, 52, 2, '2016-04-25 16:42:47', 1, 'expirado'),
(126, 52, 3, '2016-04-25 16:43:24', 1, 'expirado'),
(127, 52, 3, '2016-04-25 16:44:04', 1, 'expirado'),
(128, 58, 1, '2016-04-25 17:56:56', 2, 'expirado'),
(129, 59, 1, '2016-04-25 18:29:16', 18, 'expirado'),
(130, 1, 3, '2016-04-26 01:33:31', 4, 'cancelado'),
(131, 52, 2, '2016-04-26 01:35:50', 1, 'expirado'),
(132, 52, 1, '2016-04-26 01:36:06', 1, 'expirado'),
(133, 1, 3, '2016-04-26 01:59:42', 4, 'expirado'),
(134, 1, 1, '2016-04-18 02:24:39', 1, 'expirado'),
(135, 57, 65, '2016-04-26 03:22:04', 1, 'expirado'),
(136, 57, 55, '2016-04-26 03:23:26', 1, 'expirado'),
(137, 57, 55, '2016-04-26 03:24:22', 2, 'expirado'),
(138, 1, 1, '2016-04-28 03:29:44', 3, 'cancelado'),
(139, 1, 1, '2016-04-28 03:32:07', 5, 'realizado'),
(140, 2, 9, '2016-04-27 00:00:00', 3, 'cancelado'),
(141, 1, 2, '2016-04-28 04:47:04', 4, 'cancelado'),
(142, 1, 2, '2016-04-28 04:48:58', 1, 'cancelado'),
(143, 101, 2, '2016-04-28 04:53:07', 1, 'cancelado'),
(144, 101, 2, '2016-04-28 05:29:17', 1, 'cancelado'),
(145, 101, 3, '2016-04-28 05:30:14', 1, 'cancelado'),
(146, 101, 3, '2016-04-28 05:31:38', 2, 'activo'),
(147, 101, 3, '2016-04-28 05:32:42', 1, 'activo'),
(148, 101, 3, '2016-04-28 05:33:27', 1, 'cancelado'),
(149, 1, 4, '2016-04-28 05:42:17', 1, 'cancelado'),
(150, 102, 2, '2016-04-28 05:42:27', 3, 'activo'),
(151, 102, 3, '2016-04-28 05:42:49', 5, 'cancelado'),
(152, 102, 11, '2016-04-28 05:43:08', 2, 'realizado'),
(153, 1, 11, '2016-04-28 05:43:28', 1, 'realizado'),
(154, 1, 23, '2016-04-28 05:43:49', 5, 'realizado'),
(155, 60, 2, '2016-04-29 01:20:43', 2, 'activo');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario_vende_producto`
--

CREATE TABLE IF NOT EXISTS `usuario_vende_producto` (
  `id_venta` int(4) NOT NULL AUTO_INCREMENT,
  `id_usuario` int(4) NOT NULL,
  `id_producto` int(4) NOT NULL,
  `id_pago` int(4) NOT NULL,
  `fecha_venta` datetime NOT NULL,
  `monto` decimal(6,2) NOT NULL,
  `cantidad_venta` int(3) NOT NULL,
  PRIMARY KEY (`id_venta`,`id_usuario`,`id_producto`,`id_pago`),
  KEY `usuariovende` (`id_producto`),
  KEY `productovende` (`id_usuario`),
  KEY `productopago` (`id_pago`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci AUTO_INCREMENT=100 ;

--
-- Volcado de datos para la tabla `usuario_vende_producto`
--

INSERT INTO `usuario_vende_producto` (`id_venta`, `id_usuario`, `id_producto`, `id_pago`, `fecha_venta`, `monto`, `cantidad_venta`) VALUES
(4, 14, 68, 1, '2001-10-16 10:15:00', '35.00', 5),
(5, 18, 69, 1, '2001-10-16 10:20:00', '26.00', 5),
(6, 31, 15, 1, '2001-10-16 10:25:00', '23.00', 4),
(7, 20, 40, 1, '2001-10-16 10:30:00', '63.00', 3),
(8, 30, 67, 1, '2001-10-16 10:35:00', '12.00', 3),
(9, 18, 29, 2, '2001-10-16 10:40:00', '21.00', 4),
(10, 50, 1, 1, '2001-10-16 10:45:00', '15.00', 3),
(11, 3, 63, 1, '2001-10-16 10:50:00', '15.00', 1),
(12, 26, 20, 1, '2001-10-16 10:55:00', '50.00', 4),
(13, 8, 70, 1, '2001-10-16 11:00:00', '61.00', 4),
(14, 12, 75, 1, '2001-10-16 11:05:00', '74.00', 3),
(15, 5, 21, 1, '2001-10-16 11:10:00', '72.00', 5),
(16, 18, 71, 1, '2001-10-16 11:15:00', '54.00', 5),
(17, 49, 68, 1, '2001-10-16 11:20:00', '42.00', 1),
(18, 34, 23, 1, '2001-10-16 11:25:00', '66.00', 3),
(19, 37, 43, 1, '2001-10-16 11:30:00', '48.00', 5),
(20, 9, 54, 1, '2002-10-16 10:00:00', '59.00', 3),
(21, 27, 28, 1, '2002-10-16 10:05:00', '26.00', 2),
(22, 43, 35, 1, '2002-10-16 10:10:00', '32.00', 1),
(23, 20, 41, 1, '2002-10-16 10:15:00', '73.00', 1),
(24, 27, 33, 1, '2002-10-16 10:20:00', '45.00', 4),
(25, 35, 69, 1, '2002-10-16 10:25:00', '71.00', 5),
(26, 20, 9, 2, '2002-10-16 10:30:00', '47.00', 3),
(27, 17, 44, 1, '2002-10-16 10:35:00', '30.00', 2),
(28, 39, 3, 1, '2002-10-16 10:40:00', '34.00', 1),
(29, 43, 68, 1, '2002-10-16 10:45:00', '40.00', 2),
(30, 13, 15, 1, '2002-10-16 10:50:00', '59.00', 4),
(31, 31, 41, 1, '2002-10-16 10:55:00', '17.00', 2),
(32, 18, 73, 1, '2002-10-16 11:00:00', '52.00', 5),
(33, 29, 51, 1, '2002-10-16 11:05:00', '48.00', 2),
(34, 18, 53, 1, '2002-10-16 11:10:00', '35.00', 2),
(35, 40, 44, 1, '2002-10-16 11:15:00', '68.00', 1),
(36, 17, 71, 1, '2002-10-16 11:20:00', '65.00', 2),
(37, 48, 13, 1, '2002-10-16 11:25:00', '22.00', 3),
(38, 49, 25, 1, '2002-10-16 11:30:00', '59.00', 2),
(39, 26, 21, 1, '2003-10-16 10:00:00', '39.00', 4),
(40, 18, 52, 1, '2003-10-16 10:05:00', '23.00', 5),
(41, 13, 60, 1, '2003-10-16 10:10:00', '42.00', 2),
(42, 24, 8, 1, '2003-10-16 10:15:00', '48.00', 2),
(43, 49, 15, 1, '2003-10-16 10:20:00', '35.00', 1),
(44, 8, 43, 1, '2003-10-16 10:25:00', '32.00', 1),
(45, 35, 58, 1, '2003-10-16 10:30:00', '58.00', 2),
(46, 27, 27, 1, '2003-10-16 10:35:00', '48.00', 1),
(47, 40, 8, 1, '2003-10-16 10:40:00', '61.00', 5),
(48, 46, 9, 1, '2003-10-16 10:45:00', '16.00', 5),
(49, 44, 75, 1, '2003-10-16 10:50:00', '13.00', 2),
(50, 15, 34, 1, '2003-10-16 10:55:00', '40.00', 2),
(51, 9, 35, 1, '2003-10-16 11:00:00', '16.00', 4),
(52, 21, 46, 1, '2003-10-16 11:05:00', '13.00', 2),
(53, 35, 40, 1, '2003-10-16 11:10:00', '62.00', 1),
(54, 9, 25, 1, '2003-10-16 11:15:00', '37.00', 1),
(55, 18, 40, 1, '2003-10-16 11:20:00', '60.00', 2),
(56, 27, 46, 1, '2003-10-16 11:25:00', '34.00', 3),
(57, 12, 21, 1, '2003-10-16 11:30:00', '32.00', 1),
(58, 4, 26, 1, '2004-10-16 10:00:00', '26.00', 1),
(59, 12, 28, 2, '2004-10-16 10:05:00', '65.00', 3),
(60, 2, 39, 1, '2004-10-16 10:10:00', '32.00', 4),
(62, 36, 44, 1, '2004-10-16 10:20:00', '51.00', 1),
(63, 35, 57, 1, '2004-10-16 10:25:00', '45.00', 1),
(64, 47, 34, 1, '2004-10-16 10:30:00', '53.00', 1),
(65, 14, 33, 1, '2004-10-16 10:35:00', '59.00', 3),
(66, 4, 43, 1, '2004-10-16 10:40:00', '28.00', 1),
(67, 44, 34, 1, '2004-10-16 10:45:00', '61.00', 2),
(71, 1, 1, 1, '2016-04-26 14:41:19', '19.00', 1),
(77, 1, 1, 1, '2016-04-26 18:07:45', '19.00', 1),
(81, 1, 55, 1, '2016-04-26 18:34:45', '35.00', 1),
(82, 1, 65, 2, '2016-04-26 18:38:08', '30.00', 1),
(83, 1, 3, 1, '2016-04-26 18:38:47', '60.00', 4),
(84, 1, 1, 1, '2016-04-26 18:39:39', '19.00', 1),
(85, 1, 1, 2, '2016-04-26 18:41:17', '342.00', 18),
(89, 1, 1, 1, '2016-04-27 18:15:39', '19.00', 1),
(90, 1, 1, 1, '2016-04-27 20:45:20', '19.00', 1),
(91, 1, 12, 1, '2016-04-27 20:46:30', '60.00', 12),
(92, 1, 3, 1, '2016-04-27 20:46:53', '45.00', 3),
(97, 1, 23, 1, '2016-04-28 05:50:07', '150.00', 5),
(98, 1, 11, 1, '2016-04-28 05:52:46', '5.00', 1),
(99, 1, 11, 1, '2016-04-28 05:53:23', '10.00', 2);

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `lote`
--
ALTER TABLE `lote`
  ADD CONSTRAINT `productolote` FOREIGN KEY (`id_producto`) REFERENCES `producto` (`id_producto`),
  ADD CONSTRAINT `usuariolote` FOREIGN KEY (`id_usuario`) REFERENCES `usuario` (`id_usuario`);

--
-- Filtros para la tabla `merma`
--
ALTER TABLE `merma`
  ADD CONSTRAINT `mermaproducto` FOREIGN KEY (`id_producto`) REFERENCES `producto` (`id_producto`),
  ADD CONSTRAINT `usuariomerma` FOREIGN KEY (`id_usuario`) REFERENCES `usuario` (`id_usuario`);

--
-- Filtros para la tabla `privilegio_rol`
--
ALTER TABLE `privilegio_rol`
  ADD CONSTRAINT `privilegiorolprivilegio` FOREIGN KEY (`id_privilegio`) REFERENCES `privilegios` (`id_privilegio`),
  ADD CONSTRAINT `privilegiorolrol` FOREIGN KEY (`id_rol`) REFERENCES `roles` (`id_rol`);

--
-- Filtros para la tabla `producto`
--
ALTER TABLE `producto`
  ADD CONSTRAINT `productocategoria` FOREIGN KEY (`id_categoria`) REFERENCES `categoria` (`id_categoria`);

--
-- Filtros para la tabla `rol_usuario`
--
ALTER TABLE `rol_usuario`
  ADD CONSTRAINT `rolusuariorol` FOREIGN KEY (`id_rol`) REFERENCES `roles` (`id_rol`),
  ADD CONSTRAINT `rolusuariousuario` FOREIGN KEY (`id_usuario`) REFERENCES `usuario` (`id_usuario`);

--
-- Filtros para la tabla `usuario_aparta_producto`
--
ALTER TABLE `usuario_aparta_producto`
  ADD CONSTRAINT `producto_apartado` FOREIGN KEY (`id_producto`) REFERENCES `producto` (`id_producto`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `usuario_vende_producto`
--
ALTER TABLE `usuario_vende_producto`
  ADD CONSTRAINT `productopago` FOREIGN KEY (`id_pago`) REFERENCES `tipo_pago` (`id_pago`),
  ADD CONSTRAINT `productovende` FOREIGN KEY (`id_usuario`) REFERENCES `usuario` (`id_usuario`),
  ADD CONSTRAINT `usuariovende` FOREIGN KEY (`id_producto`) REFERENCES `producto` (`id_producto`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
